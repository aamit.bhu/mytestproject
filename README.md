 @Test
    void deleteEntitlement() {
        String  entitlementId = "121";
        Entitlement entitlement = new Entitlement();
        entitlement.setId(entitlementId);
        when(this.entitlementRepository.findById(entitlementId)).thenReturn(Optional.of(entitlement));
        this.entitlementInterface.deleteEntitlement(entitlementId);
        verify(this.entitlementRepository, times(1)).deleteById(entitlementId);
    }
